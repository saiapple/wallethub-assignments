import { CommonModule, CurrencyPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FirstPageRoutingModule } from './first-page-routing.module';
import { FirstPageComponent } from './first-page.component';
import { FormComponent } from './form/form.component';


@NgModule({
  declarations: [FirstPageComponent, FormComponent],
  imports: [
    CommonModule,
    FirstPageRoutingModule,
    FormsModule,
  ],
  providers: [CurrencyPipe]
})
export class FirstPageModule { }
