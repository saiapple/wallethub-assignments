import { CurrencyPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-first-page',
  templateUrl: './first-page.component.html',
  styleUrls: ['./first-page.component.scss']
})
export class FirstPageComponent implements OnInit {
  saving: any = 123123;

  constructor(private currencyPipe: CurrencyPipe) {
  }

  ngOnInit() {
  }

  transformAmount(amount) {
    this.saving = parseFloat(amount);
    this.saving = this.currencyPipe.transform(this.saving, '$');
  }

}
