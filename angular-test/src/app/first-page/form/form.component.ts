import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from 'protractor';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  @Input() saving: number;
  @Output() savingChange = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  transformAmount() {
    this.savingChange.emit(this.saving.toString());
  }
}
